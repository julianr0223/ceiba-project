package com.lab_ingreso_project_4.app.limonade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LimonadeApplication {

	public static void main(String[] args) {
		SpringApplication.run(LimonadeApplication.class, args);
	}
}
